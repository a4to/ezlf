#!/usr/bin/env bash

# Basic vars
set previewer lf-ueberzug-previewer
set cleaner lf-ueberzug-cleaner
set icons
set period 1
set hiddenfiles ".*:*.aux:*.log:*.bbl:*.bcf:*.blg:*.run.xml"
set shell bash
set shellopts '-eu'
set ifs "\n"
set scrolloff 10
set drawbox true

# cmds/functions
cmd open ${{
    case $(file --mime-type $f -b) in
	image/vnd.djvu|application/pdf|application/octet-stream) setsid -f zathura $fx >/dev/null 2>&1 ;;
        text/*) $EDITOR $fx;;
	image/x-xcf) setsid -f gimp $f >/dev/null 2>&1 ;;
	image/svg+xml) display -- $f ;;
	image/*) rotdir $f | grep -i "\.\(png\|jpg\|jpeg\|gif\|webp\|tif\|ico\)\(_large\)*$" | sxiv -aio 2>/dev/null | lf-select ;;
	audio/*) mpv --audio-display=no $f ;;
	video/*) setsid -f mpv $f -quiet >/dev/null 2>&1 ;;
	application/pdf|application/vnd*|application/epub*) setsid -f zathura $fx >/dev/null 2>&1 ;;
	application/pgp-encrypted) $EDITOR $fx ;;
        *) for f in $fx; do setsid -f $OPENER $f >/dev/null 2>&1; done;;
    esac
}}

cmd mkdir $mkdir -p "$(echo $* | tr ' ' '\ ')"

cmd extract ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	printf "%s\n\t" "$fx"
	printf "extract?[y/N]"
	read ans
	[ $ans = "y" ] && ext $fx
}}

cmd delete ${{
	clear; tput cup $(($(tput lines)/1)); tput bold
	set -f ; clear ; echo -e "\\n\\n" ; printf "%s\n\t" "$fx" ;
  echo -en \\n\\n"$(tput bold; tput setaf 6)   Delete selected items?$(tput setaf 6) $(tput setaf 1)($(tput setaf 3)Y/n$(tput setaf 1)) $(tput setaf 6): "$(tput setaf 1)""; read ans
    [[ $ans = "y" ]] || [[ $ans = Y ]] || [[ $ans = "" ]] || [[ $ans = yes ]] || 
      [[ $ans = Yes ]] || [[ $ans = YES ]] && rm -rf -- $fx ; tput sgr0 || exit 0 ; tput sgr0   
}}

cmd rootdel ${{
  clear; tput cup $(($(tput lines)/1)); tput bold
	set -f ; clear ; echo -e "\\n\\n" ; printf "%s\n\t" "$fx" ;
  echo -en \\n\\n"$(tput bold; tput setaf 6)   ARE YOU SURE YOU WANT TO DELETE THE SELECTED ITEMS?$(tput setaf 6) $(tput setaf 1)($(tput setaf 3)y/N$(tput setaf 1)) $(tput setaf 6): "$(tput setaf 1)""; read ans
    [[ $ans = "y" ]] || [[ $ans = Y ]] || [[ $ans = yes ]] || [[ $ans = Yes ]] || 
      [[ $ans = YES ]] && sudo rm -rf -- $fx ; tput sgr0 || exit 0 ; tput sgr0
}}

cmd moveto ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear; echo "Move to where?"
	dest="$(awk '{ gsub(/#.*/, ""); } !/^$/ { print $2 }' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs | fzf)" &&
	for x in $fx; do
		eval mv -iv \"$x\" \"$dest\"
	done &&
	notify-send "🚚 File(s) moved." "File(s) moved to $dest."
}}

cmd copyto ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear; echo "Copy to where?"
	dest="$(awk '{ gsub(/#.*/, ""); } !/^$/ { print $2 }' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs | fzf)" &&
	for x in $fx; do
		eval cp -ivr \"$x\" \"$dest\"
	done &&
	notify-send "📋 File(s) copied." "File(s) copies to $dest."
}}

cmd symlink ${{
	clear ; dest="$PWD" ; for x in $fx; do
	  eval ln -sf \"$x\" \"$dest\"
  done && notify-send "🔗 File(s) symlinked".\
  "File(s) symlinked to $dest."
}}

cmd symlink_rel ${{
	clear ; dest="$PWD" ; for x in $fx; do
	  eval ln -srf \"$x\" \"$dest\"
  done && notify-send "🔗 File(s) symlinked".\
  "File(s) symlinked to $dest."
}}

cmd symlink_hard ${{
	clear ; dest="$PWD" ; for x in $fx; do
	  eval ln -f \"$x\" \"$dest\"
  done && notify-send "💪 File(s) hard linked".\
  "File(s) hard linked to $dest."
}}

cmd cloneRepo ${{
  clear; tput cup $(($(tput lines)/3)); fullurl=$(mktemp) ; trap 'rm -f $fullurl' \
  EXIT INT TERM QUIT STOP HUP ERR ; set -f ; url=$(dialog --title "Git Clone" --inputbox \
  " Please enter the URL of the repo you would like to clone: " 10 65 3>&1 1>&2 2>&3 3>&1 &&
  notify-send "📦Cloning repo" "You will be notified upon completion.") ; echo $url|read url 
  echo "$(echo $url|sed 's/http:\/\///g;s/https:\/\///g')">$fullurl && url=$(cat $fullurl) && 
  notify='📦Repo successfully cloned into' ; eval $(git clone git@$url >/dev/null 2>&1 && 
  notify-send "$notify $(basename $url)" || git clone https://$url >/dev/null 2>&1 &&
  notify-send "$notify $(basename $url .git)" || notify-send "📦 Repo clone failed.") & disown 
}}

cmd modeChange ${{
  sudo chmod -R $1 $fx && 
    for x in ${fx[@]}; do
      notify-send "🔒 Permission of $(basename ${x}) changed to $1"
    done
}}


cmd meChange ${{
  ! sudo chown "nvx1:nvx1" -Rf ${*} && notify-send "🔧 Owning User Changed to nvx1 &&
    notify-send "🔧 Ownership Change Failed"
}}

cmd mpvPlay ${{
  mpv --audio-display=no $f >/dev/null 2>&1 &
  notify-send "🎶  Playing $(basename $f)"
}}

cmd newFile ${{
  file=$(dialog --title ' ~ New file ~ ' --inputbox \
  " Enter a name for the file: " 10 60 3>&1 1>&2 2>&3 3>&1) && echo $file|read file
  [ -e $file ] && notify-send "❌ Error, $file already exists in this directory."&& exit 1 ||
  touch $file && notify-send "📝 File: \"${file}\"          Created successfully!"
}}

cmd sudoCP ${{
dest=$PWD
eval "$(sudo cp -r $fx $dest && notify-send "📋 File(s) copied." "File(s) copied to $dest.")" & disown
}}

cmd sudoMV ${{
dest=$PWD
eval "$(sudo mv $fx $dest && notify-send "📋 File(s) moved." "File(s) moved to $dest.")" & disown
}}

cmd shellScript ${{
  script=$(dialog --title ' ~ New Shell Script ~ ' --inputbox \
  " Enter a name for the script: " 10 60 3>&1 1>&2 2>&3 3>&1) && echo $script|read script
  [ -e $script ] && notify-send "❌ Error, $script already exists in this directory."&& exit 1 ||
  echo "#!/usr/bin/env bash" > $script && chmod +x $script && eval $(${TERMINAL:-st} -e ${EDITOR:-vim} \
  $PWD/$script >/dev/null 2>&1 && notify-send "📝 Shell script created!")
}}


cmd makeTemp ${{
tmpd=$(echo $$) ; eval $(mkdir $tmpd && cd $tmpd && notify-send "🔧 Temp dir created." ||
    notify-send "❌ Could not create temp dir.")
}}


cmd quitcd ${{
	echo "1" > "$EZLF_TMP/changecwd"
	quit
}}

cmd setbg "$1"
cmd bulkrename $vidir

cmd quitcd ${{
	echo "1" > "$LF_UEBERZUG_TEMPDIR/changecwd"
	lf -remote "send $id quit"
}}


  #-----------------------#
 #  ~  B I N D I N G S  ~  #
  #-----------------------#


# Basic Bindings:

map gh
map g top
map c cut
map d delete
map - delete
map a rename 
map <delete> delete
map <c-r> reload
map <backspace2> set hidden!
map <enter> shell
map o &mimeopen $f
map O $mimeopen --ask $f


# Custom Functions:

map Q quitcd
map m mpvPlay
map t newFile
map B bulkrename
map E extract
map C sudoCP
map M moveto
map D rootdel
map L symlink
map R symlink_rel
map H symlink_hard
map <c-g> cloneRepo
map <c-b> shellScript
map <c-t> makeTemp
map <c-^> modeChange 644 # Ctrl + 6
map <c-_> modeChange 755 # Ctrl + 7
map = meChange $fx


# Commmand Oriented Bindings:

map x !$f
map s !nvim $f
map C !sudo cut
map z !sxiv $f
map Z !sxiv *
map V !mpv *
map S !sudo nvim $f
map X !chmod +x $f !$f
map b !xwallpaper --stretch $f
map <c-v> !mpv --shuffle *
map <a-v> !mpv $f
map <c-s> :!nvim $? 
map <c-e> !chmod +x $f
map <c-y> :!mpv --shuffle $PWD
map <c-y> :!mpv --shuffle $PWD
map <f-9> :!nvim<CR>
map A !chmod +x *


# Push Bindings:

map e push :!nvim<space> 
map n push :mkdir<space>
map N push :sudo mkdir<space>
map I push A<c-a> # at the very beginning
map i push A<a-b><a-b><a-f> # before extention
map A push A<a-b> # after extention


