# An lf wrapper, with built in previews and on-exit directory changing.


`lfcd` is a simple wrapper for the `lf` file manager, with built in file, image and video previews, as well as on-exit directory changing.

The program makes use of `ueberzug` to preview images, documents and video thumbnails. Preview behavior can be altered by editing the *scope* file.



## Usage:

Run `lfcd`. Optionally, a startup directory can be specified by a second argument. By default, `lfcd` will launch in your current working directory.


**Prerequisites**

+ `lf`: the file manager                    
+ `zsh`: for scripting                      
+ `ffmpeg`: for video file thumbnails       
+ `ueberzug`: for image previews            
+ `graphicsmagick`: for svg and gif previews
+ `ghostscript`: for pdf previews 
